# DevOps Final Project

## Task description
### Mission
The goal for the project is to build application that's scalable and reliable. You're going to use Kubernetes to achieve that.

### What kind of application
You'll need to come up with an application that's has just one meaningful function. Good examples of those are:
- [x] Machine learning application that's accepts new examples and returns predictions for them
- [ ] Application that solves one of the leetcode problems

Please note that the application itself does not have to be hard to implement because we're focusing on the DevOps part here
The main function of your application is going to be exposed as web application such as it clients are able to use it with curl or requests Python library.
You must have two implementations of the main function to your application. We're going to call the first one The Proper Solution and the second one The Fallback Solution

## Implementation

### Application
The application in docker container provides API using Fast API, two methods are implemented:

| Method | Parameters                                                                                                                                                                                                                                               | Return                                      |
| ------ |----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------|
|`/make_prediction`| - `interval`: (days, weeks, month), default weeks <br/>- `prediction_period`: int - number of intervals to predict after end_dt, default - 4<br/>- `start_dt`: (YYYY-MM-DD) start of train data<br/>- `end_dt`: (YYYY-MM-DD) end of train, default today | `{'date': (lower bound, price, upper bound)}` |
|`/predict_constant`| "Fallback solution" returns constant prediction from `data/constant_prediction.json`                                                                                                                                                                     | `{'date': (lower bound, price, upper bound)}` |

**Model**: [Prophet](https://facebook.github.io/prophet/) by Facebook.<br/>
**Data**: from Yahoo Finance using [yfinance](https://github.com/ranaroussi/yfinance) library

### Infrastrucre
1. The Application can be run in a docker container using `Dockerfile` or you can pull image from project's registry.
2. There is a `Vagrantfile` in infra directory which launches VM using `virtualbox`. The VM is provisioned by Ansible.
3. In `playbook.yml` the infrastructure is being prepared to run the application in `minikube`.
4. For running application there are k8s manifest files - `manifest_*.yml`.

## Quick start
1. Clone repo on your machine - `git clone ...`.
2. Install Ansible using `pip intall ansible` and `vagrant`.
3. Enter `infra` directory. `cd infra`
4. Decrypt `vault.yml` with password. `ansible-vault decrypt vault.yml`
3. Run `vagrant up`. Wait until it's done. For me it took 5 minutes.
4. Log in VM using `vagrant ssh`.
6. Run `curl currency-app.mds/`, `curl currency-app.mds/predit_constant`, `curl currency-app.mds/make_predictions`. If you want, specify parameters for the methods from the table above.
