import os
import json
from datetime import date, timedelta
from typing import Optional

from fastapi import FastAPI

from src.data.data_preparation import get_history_data
from src.models.prophet_model import ProphetModel
from src.utils import df_to_dict

app = FastAPI()


@app.get('/')
async def root():
    return {"message": "Hello World"}


@app.get('/make_prediction')
async def make_prediction(
        interval: str = 'weeks',
        prediction_period: int = 4,
        start_dt: Optional[str] = None,
        end_dt: Optional[str] = None
):
    match interval:
        case 'days':
            predict_interval = 'B'
            history_interval = '1d'
        case 'weeks':
            predict_interval = 'W'
            history_interval = '1wk'
        case 'months':
            predict_interval = 'MS'
            history_interval = '1mo'

    data = get_history_data(
        history_interval=history_interval,
        start_dt=start_dt,
        end_dt=end_dt
    )

    model = ProphetModel()
    model.train(data)
    prediction = df_to_dict(model.predict(prediction_period, interval=predict_interval))
    return {'data': prediction}


@app.get('/predict_constant')
async def predict_constant(
        interval: str = 'weeks',
        prediction_period: int = 4
):
    with open(os.path.join(os.getcwd(), 'data/constant_prediction.json')) as file:
        const_pred = json.load(file)

    match interval:
        case 'days':
            start_dt = date.today()
        case 'weeks':
            start_dt = date.today() - timedelta(days=date.today().weekday())

    dt = [start_dt + timedelta(**{interval: p}) for p in range(1, prediction_period + 1)]
    values = [const_pred.get(interval)] * prediction_period

    return {'data': dict(zip(dt, values))}
