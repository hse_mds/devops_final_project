HISTORY_INTERVALS = (
    # '1m', '2m', '5m', '15m', '30m', '60m', '90m', '3mo', '1h',
    '1d', '1wk', '1mo'
)

PROPHET_PARAMS = {
    'changepoint_prior_scale': 0.48,
    'seasonality_prior_scale': 5.47,
    'seasonality_mode': 'multiplicative'
}
