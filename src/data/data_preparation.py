from typing import Optional
from datetime import date, timedelta

import yfinance as yf

from constants import HISTORY_INTERVALS


def get_history_data(
        ticker_name: str = 'RUB=X',
        history_interval: str = '1wk',
        start_dt: Optional[str] = None,
        end_dt: Optional[str] = None
):
    ticker = yf.Ticker(ticker_name)

    if history_interval not in HISTORY_INTERVALS:
        raise ValueError(f'Invalid history_interval,\nExpected one from {HISTORY_INTERVALS}\nGot {history_interval}')
    if end_dt:
        try:
            end_dt = date.fromisoformat(end_dt)
        except ValueError as err:
            print(f'Invalid date format of `end_dt`,\nExpected YYYY-MM-DD. Got {end_dt}\nSetting default value.')
            end_dt = date.today()
    else:
        end_dt = date.today()

    if start_dt:
        try:
            start_dt = date.fromisoformat(start_dt)
        except ValueError:
            raise ValueError(f'Invalid date format of `start_dt`,\nExpected YYYY-MM-DD. Got {start_dt}')
    else:
        match history_interval:
            case '1d':
                start_dt = end_dt - timedelta(days=365)
            case '1wk':
                start_dt = end_dt - timedelta(days=730)
            case '1mo':
                start_dt = end_dt - timedelta(days=1500)

    history = ticker.history(
        interval=history_interval,
        start=start_dt,
        end=end_dt,
        actions=False,
        raise_errors=False
    )

    return history

