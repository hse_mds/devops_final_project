from prophet import Prophet

from constants import PROPHET_PARAMS


class ProphetModel:
    def __init__(self):
        self.model = Prophet(**PROPHET_PARAMS)

    @staticmethod
    def _prepare_data(data):
        train_data = data.Close.reset_index().rename(columns={'Date': 'ds', 'Close': 'y'})
        train_data['ds'] = train_data['ds'].dt.tz_localize(None)
        return train_data

    def train(self, history_data):
        self.model.fit(self._prepare_data(history_data))

    def predict(self, periods_predict: int = 4, interval: str = 'W'):
        future = self.model.make_future_dataframe(periods_predict, freq=interval)
        forecast = self.model.predict(future)
        return forecast[['ds', 'yhat_lower', 'yhat', 'yhat_upper']].iloc[-periods_predict:]

