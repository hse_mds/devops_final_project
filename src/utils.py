def df_to_dict(df):
    result = {}
    for _, row in df.iterrows():
        result[row.ds] = (round(row.yhat_lower, 2), round(row.yhat, 2), round(row.yhat_upper, 2))
    return result
